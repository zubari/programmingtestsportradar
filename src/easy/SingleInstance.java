package easy;

public class SingleInstance {

	/**
	 * This method should always return the same instance of SingleInstance
	 * 
	 * @return An instance of SingleInstance
	 */
	public static SingleInstance getInstance() {
		return null;
	}

	public SingleInstance() {
		try {
			Thread.sleep(100);
		}
		catch (InterruptedException e) {
			return;
		}
	}

}
