package easy;

public class Rectangle {
    private float width, length;

    public void createRectangle(float width, float length){
        this.width = width;
        this.length = length;
    }

    public float getWidth() {
        return width;
    }

    public float getLength() {
        return length;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public void setLength(float length){
        this.length = length;
    }

    public float getArea(){
        return this.width * this.length;
    }

    public float getCircumference(){
        return 2 * (this.width + this.length);
    }
}
