package easy;

public class Cube extends Rectangle{

    private float height;

    public void createCube(float width, float length, float height) {
        createRectangle(width,height);
        this.height = height;
    }

    public float getVolume()
    {
        return getArea() * height;
    }
}
