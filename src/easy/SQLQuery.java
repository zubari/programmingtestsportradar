package easy;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class SQLQuery {
    private Connection connection;
    private Statement statement;
    private ResultSet resultSet;

    public SQLQuery(){
        try{
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/programmingtest?useUnicode=true&useSSL=false&useLegacyDatetimeCode=false&serverTimezone=Turkey", "root", ""); //serverTimezone=Turkey indicates server's local timezone, change accordingly.
            statement = connection.createStatement();

        }catch (Exception exception){
            System.out.println("Connection exception: " + exception);
        }
    }

    public void matchesInSeason(){
        try {
            String query = "SELECT * FROM `match` WHERE seasonid = 1 ORDER BY dateofmatch DESC";
            resultSet = statement.executeQuery(query);
            System.out.println("Matches recorded in the database:");

            while (resultSet.next()){
                String id = resultSet.getString("id");
                String dateofmatch = resultSet.getString("dateofmatch");
                String teamidhome = resultSet.getString("teamidhome");
                String teamidaway = resultSet.getString("teamidaway");
                String seasonid = resultSet.getString("seasonid");

                String homeTeamName = getTeamName(Integer.parseInt(teamidhome));
                String awayTeamName = getTeamName(Integer.parseInt(teamidaway));
                String seasonName = getSeasonName(Integer.parseInt(seasonid));

                System.out.println("Match ID: "+id+" Date of match: "+dateofmatch+" Home team: "+homeTeamName+" Away team: "+awayTeamName+" Season: "+seasonName);
            }

        }catch (Exception exception){
            System.out.println("Select exception: " + exception);
        }
    }

    /**
     * Gets the team name where teamid is provided.
     *
     * @param teamID - id of the team.
     * @return team name as String.
     */
    public String getTeamName(int teamID){

        try {
            String homeTeamName = "SELECT name FROM `team` WHERE id = "+teamID;
            statement = connection.createStatement();
            ResultSet nameResultSet = statement.executeQuery(homeTeamName);

            String teamName = "";
            while (nameResultSet.next()){
                teamName = nameResultSet.getString("name");
            }

            return teamName;

        }catch (Exception exception){
            System.out.println("Select exception: " + exception);
        }
        return "Not found.";
    }

    /**
     * Gets the season name where seasonid is provided.
     *
     * @param seasonID - id of the team.
     * @return team name as String.
     */
    public String getSeasonName(int seasonID){
        try {
            String homeTeamName = "SELECT name FROM `season` WHERE id = "+seasonID;
            statement = connection.createStatement();
            ResultSet nameResultSet = statement.executeQuery(homeTeamName);

            String seasonName = "";
            while (nameResultSet.next()){
                seasonName = nameResultSet.getString("name");
            }

            return seasonName;

        }catch (Exception exception){
            System.out.println("Select exception: " + exception);
        }
        return "Not found.";
    }
}
