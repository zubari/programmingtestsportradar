package easy;

public class FizzBuzz {

	public static void fizzBuzz() {

		int num = 101;
		for (int i = 1; i < num; i++)
		{
			boolean printed = false;

			if (i % 3 == 0)
			{
				printed = true;
				System.out.print("Fizz");
			}
			if (i % 5 == 0)
			{
				printed = true;
				System.out.print("Buzz");
			}

			if (Integer.toString(i).contains("3"))
			{
				printed = true;
				System.out.print("Bizz");
			}
			if (Integer.toString(i).contains("5"))
			{
				printed = true;
				System.out.print("Fuzz");
			}

			if (!printed) System.out.print(i);
			System.out.println();
		}
	}

	public static void main(String[] args) {
		FizzBuzz.fizzBuzz();
	}

}
