package medium;

public class Player implements Spaceship {
    private float hull, shield, damage;

    @Override
    public void setHull(float hull) {
        this.hull = hull;
    }

    @Override
    public void setShield(float shield) {
        this.shield = shield;
    }

    @Override
    public void setDamage(float damage) {
        this.damage = damage;
    }

    @Override
    public float getHull() {
        return 0;
    }

    @Override
    public float getShield() {
        return 0;
    }

    @Override
    public float getDamage() {
        return 0;
    }

    @Override
    public void shoot(Enemy enemy) {
        if(enemy.getShield() > 0){
            enemy.setShield(enemy.getShield()-damage);
            System.out.println("Player shoots enemy. Current shiels is at: "+ enemy.getShield());
        }
        else if(enemy.getHull() > 0)
            enemy.setHull(enemy.getHull()-damage);
    }

    public static void main(String[] args){
        Enemy enemy1 = new Enemy();
        enemy1.setHull(10);
        enemy1.setShield(10);
        enemy1.setDamage(1);
        enemy1.printCharacteristics();

        Enemy enemy2 = new Enemy();
        enemy2.setHull(13);
        enemy2.setShield(16);
        enemy2.setDamage(2);
        enemy2.printCharacteristics();

        Player player = new Player();
        player.setHull(15);
        player.setShield(20);
        player.setDamage(2);
        player.printCharacteristics();

        while(enemy1.getHull() > 0){
            player.shoot(enemy1);
        }
    }

    public void printCharacteristics(){
        System.out.println("Hull: "+this.hull+" Shield: "+ this.shield + " Damage: "+ this.damage);
    }
}
