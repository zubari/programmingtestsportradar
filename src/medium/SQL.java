package medium;

import java.sql.*;

public class SQL {
    //SELECT * FROM table1 WHERE id IN (SELECT id FROM table2 ORDER BY num1+num2 DESC LIMIT 5)
    private Connection connection;
    private Statement statement;
    private ResultSet resultSet;

    public SQL(){
        try{
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/programmingtest?useUnicode=true&useSSL=false&useLegacyDatetimeCode=false&serverTimezone=Turkey", "root", ""); //serverTimezone=Turkey indicates server's local timezone, change accordingly.
            statement = connection.createStatement();

        }catch (Exception exception){
            System.out.println("Connection exception: " + exception);
        }
    }

    public void selectTop5(){
        try {
            String query = "SELECT COUNT(teamidhome) AS count FROM `match` GROUP BY teamidhome ORDER BY count DESC limit 5";
            //select count(*) c from emp1 group by name order by c desc limit 1
            resultSet = statement.executeQuery(query);

            while (resultSet.next()){
                String teamidhome = resultSet.getString("teamidhome");
                String homeTeamName = getTeamName(Integer.parseInt(teamidhome));

                int count = 0;
                String countQuery = "SELECT COUNT(teamidhome) FROM `match` WHERE teamidhome ="+ teamidhome;
                PreparedStatement preparedStatement = connection.prepareStatement(countQuery);
                ResultSet rs = preparedStatement.executeQuery();
                while(rs.next()){
                    count = rs.getInt(1);
                }

                System.out.println("Team ID: "+teamidhome+" Team name: "+homeTeamName+" Number of matches played: " + count);
            }

        }catch (Exception exception){
            System.out.println("Select exception: " + exception);
        }
    }

    public String getTeamName(int teamID){

        try {
            String homeTeamName = "SELECT name FROM `team` WHERE id = "+teamID;
            statement = connection.createStatement();
            ResultSet nameResultSet = statement.executeQuery(homeTeamName);

            String teamName = "";
            while (nameResultSet.next()){
                teamName = nameResultSet.getString("name");
            }

            return teamName;

        }catch (Exception exception){
            System.out.println("Select exception: " + exception);
        }
        return "Not found.";
    }
}
