package medium;

import com.sun.xml.internal.ws.util.StringUtils;

import java.util.List;

public class StringUtil {

	/**
	* Removes all spaces and dots (.) surrounding the text
	* (' ... Test    .    ' => 'Test')
	*/
	public static String makeNice(String text) {
		text = text.replaceAll("\\s+", "");
		text = text.replaceAll("\\.", "");
		return text;
	}

	/**
	  * Explodes a string into a list of substrings which in the original string is
	  * separated by the given separator
	  */
	public List<String> explodeText(String text, String separator) {
        String[] seperated = text.split("separator");
		return null;
	}

	/**
	 * Makes a string's first character uppercase
	 */
	public static String uppercaseFirst(String text) {
        String upperCase = text.substring(0, 1).toUpperCase() + text.substring(1);
        //String upperCase = StringUtils.capitalize(text);  Or simply this in spring framework.
		return text;
	}

	/**
	 * Uppercase the first character of each word in a string
	 */
	public static String uppercaseWords(String text) {
        String[] seperatedWords = text.split(" ");
        StringBuffer stringBuffer = new StringBuffer();

        for (String seperatedWord : seperatedWords) {
            stringBuffer.append(Character.toUpperCase(seperatedWord.charAt(0))).append(seperatedWord.substring(1)).append(" ");
        }
		return stringBuffer.toString().trim();
	}

	/**
	 * Returns a list of all integers in the given text
	 */
	public int[] extractIntegers(String text) {
        String replacedText = text.replaceAll("[\\D]+"," ");
        String[] integers = replacedText.split(" ");
        int[] integerList = new int[integers.length];
        for(int i=0;i<integers.length;i++){
            try{
                integerList[i] = Integer.parseInt(integers[i]);
            }
            catch( Exception exception ) {
                System.out.println("Parse exception: "+ exception);
            }
        }
		return integerList;
	}

}
