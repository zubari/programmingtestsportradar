package medium;

public class Enemy implements Spaceship {
    private float hull, shield, damage;

    @Override
    public void setHull(float hull) {
        this.hull = hull;
    }

    @Override
    public void setShield(float shield) {
        this.shield = shield;
    }

    @Override
    public void setDamage(float damage) {
        this.damage = damage;
    }

    @Override
    public float getHull() {
        return hull;
    }

    @Override
    public float getShield() {
        return shield;
    }

    @Override
    public float getDamage() {
        return damage;
    }

    @Override
    public void shoot(Enemy enemy) {
        if(enemy.getShield() > 0)
            enemy.setShield(enemy.getShield()-damage);
        else if(enemy.getHull() > 0)
            enemy.setHull(enemy.getHull()-damage);
        else System.out.println("Enemy eliminated.");
    }

    public void printCharacteristics(){
        System.out.println("Hull: "+this.hull+" Shield: "+ this.shield + " Damage: "+ this.damage);
    }
}
