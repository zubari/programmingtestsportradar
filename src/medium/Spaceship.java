package medium;

public interface Spaceship {
    void setHull(float hull);
    void setShield(float shield);
    void setDamage(float damage);
    float getHull();
    float getShield();
    float getDamage();
    void shoot(Enemy enemy);
}
