package hard;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Improve {

	private final Map<String, Integer> attributes = new HashMap<String, Integer>();

	//There is something cheesy along these lines but I can't tell what exactly. Hashing normally increases the iteration speed.
	public Improve() {
		this.attributes.put("users.Lucas.number", 5);
		this.attributes.put("users.Eve.number", 15);
		this.attributes.put("users.Alice.number", 13);
		this.attributes.put("users.Bob.number", 7);
	}

	public synchronized Integer userLuckyNumber(String name, int j) throws InterruptedException {

		Long hugeNumber = factorial(2 * j);
		System.out.println("Huge number from (2 * " + j + ") is: " + hugeNumber);

		String key = "users." + name + ".number";

		Integer number = attributes.get(key);

		if (number == null) {
			return -1;
		}
		else {
			return number;
		}
	}

	private long factorial(int n) throws InterruptedException {
		//Thread.sleep(500); what is the point of the sleep here, other than to slow the execution? :D
        //I don't know what else I can do, maybe I can implement a faster factorial calculation but it is pretty fast as is.

        long result = 1;
		for (int i = 1; i <= n; i++) {
			result *= i;
		}

		return result;
	}

	//I actually don't have any experience with multi-threading.
	public static void main(String[] args) {
        final long startTime = System.currentTimeMillis();
		try {
			final Improve imp = new Improve();

			ExecutorService exec = Executors.newCachedThreadPool();

			final int N = 5;

			final CyclicBarrier barrier = new CyclicBarrier(N);

			for (int i = 1; i <= N; ++i) {
				final int j = i;
				Runnable runner = new Runnable() {
					public void run() {
						try {
							barrier.await();

							for (int k = 1; k <= N; k++) {

								System.out.println("\nThread number: " + j);

								if ((j * k) % 2 == 0) {
									System.out.println("Lucas: " + imp.userLuckyNumber("Lucas", j));
								}

								if ((j * k) % 3 == 0) {
									System.out.println("Eve: " + imp.userLuckyNumber("Eve", j));
								}

								if ((j * k) % 5 == 0) {
									System.out.println("Alice: " + imp.userLuckyNumber("Alice", j));
								}

								if ((j * k) % 7 == 0) {
									System.out.println("Bob: " + imp.userLuckyNumber("Bob", j));
								}
							}
						}
						catch (Exception e) {
							e.printStackTrace();
						}
					}
				};

				exec.execute(runner);
			}

			exec.shutdown();
			exec.awaitTermination(10, TimeUnit.SECONDS);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		long elapsedTime = System.currentTimeMillis() - startTime;
        System.out.println("Elapsed time: "+ elapsedTime);
	}
}
