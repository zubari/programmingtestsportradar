package hard;
 /*
 * Function Q is defined as
 *
 *   q(1) = 1
 *   q(2) = 1
 *   q(n) = q(n - q(n - 1)) + q(n - q(n - 2))  for  n > 2.
 *
 * Write function q to calculate q(n)
 *
 * Calculate q(77)
 *
 */

public class Q {
	
	public static int q(int n) {

		int[] sequence;
		sequence = new int[n];
		sequence[0] = 1;
		sequence[1] = 1;

		for (int i = 2; i < sequence.length; i++) {
			sequence[i] = sequence[i - sequence[i - 1]] + sequence[i - sequence[i - 2]];
		}
		return sequence[n - 1];
	}

	public static void main(String[] args) {
		System.out.println("q(77) = " + q(77));
	}

}
