package hard;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * I know that I should use Inner Join in this query but I don't have much time lest, so I'll write what comes to my mind first.
 */
public class HardSQL {
    private Connection connection;
    private Statement statement;
    private ResultSet resultSet;

    HardSQL(){

        try{
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/programmingtest?useUnicode=true&useSSL=false&useLegacyDatetimeCode=false&serverTimezone=Turkey", "root", ""); //serverTimezone=Turkey indicates server's local timezone, change accordingly.
            statement = connection.createStatement();

        }catch (Exception exception){
            System.out.println("Connection exception: " + exception);
        }
    }

    public void showMatches(){
        try {
            String query = "SELECT m.dateofmatch, m.hometeamid, m.awayteamid, t.countryid, s.seasonid, t.tournamentid* " +
                    "FROM Match m INNER JOIN Season s ON (m.season id = s.id) INNER JOIN  Tournament t ON (s.tournamentid = t.id) " +
                    "WHERE m.dateofmatch BETWEEN `2012-09-18 00:00:01` AND `201211-19 00:00:01` ORDER BY m.dateofmatch ASC";
            resultSet = statement.executeQuery(query);
            System.out.println("Matches recorded in the database:");

            while (resultSet.next()){
                String dateofmatch = resultSet.getString("dateofmatch");
                String teamidhome = resultSet.getString("teamidhome");
                String teamidaway = resultSet.getString("teamidaway");
                String seasonid = resultSet.getString("seasonid");

                String homeTeamName = getTeamName(Integer.parseInt(teamidhome));
                String awayTeamName = getTeamName(Integer.parseInt(teamidaway));
                String seasonName = getSeasonName(Integer.parseInt(seasonid));
            }

        }catch (Exception exception){
            System.out.println("Select exception: " + exception);
        }
    }

    /**
     * Gets the team name where teamid is provided.
     *
     * @param teamID - id of the team.
     * @return team name as String.
     */
    public String getTeamName(int teamID){

        try {
            String homeTeamName = "SELECT name FROM `team` WHERE id = "+teamID;
            statement = connection.createStatement();
            ResultSet nameResultSet = statement.executeQuery(homeTeamName);

            String teamName = "";
            while (nameResultSet.next()){
                teamName = nameResultSet.getString("name");
            }

            return teamName;

        }catch (Exception exception){
            System.out.println("Select exception: " + exception);
        }
        return "Not found.";
    }

    /**
     * Gets the season name where seasonid is provided.
     *
     * @param seasonID - id of the team.
     * @return team name as String.
     */
    public String getSeasonName(int seasonID){
        try {
            String homeTeamName = "SELECT name FROM `season` WHERE id = "+seasonID;
            statement = connection.createStatement();
            ResultSet nameResultSet = statement.executeQuery(homeTeamName);

            String seasonName = "";
            while (nameResultSet.next()){
                seasonName = nameResultSet.getString("name");
            }

            return seasonName;

        }catch (Exception exception){
            System.out.println("Select exception: " + exception);
        }
        return "Not found.";
    }
}
